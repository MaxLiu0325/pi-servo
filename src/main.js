const raspi = require('raspi');
const I2C = require('raspi-i2c').I2C;
const sleep = require('sleep');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const addr = 0x40;

raspi.init(() => {
	const i2c = new I2C();

	i2c.writeByteSync(addr, 0, 0x20);
	i2c.writeByteSync(addr, 0xfe, 0x1e);

	i2c.writeWordSync(addr, 0x06, 0);
	i2c.writeWordSync(addr, 0x08, 1250);

	i2c.writeWordSync(addr, 0x0a, 0);
	i2c.writeWordSync(addr, 0x0c, 1250);

	app.post('/piServo', (req, res) => {
		set(i2c, req.body.pan, req.body.tilt);
	});
});

function scale(x, in_min, in_max, out_min, out_max) {
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

function set(i2c, p, t) {
  const pan = parseInt(scale(p, 0, 100, 428, 1807));
  const tilt = parseInt(scale(t, 0, 100, 424, 1741));

  console.log(`${p} : ${t}`+' ', `${pan} / ${tilt}`);

	i2c.writeWordSync(addr, 0x08, pan);
	i2c.writeWordSync(addr, 0x0c, tilt);
}
